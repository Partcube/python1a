parolalar = []
while True:
    print('Bir işlem seçin')
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')
    if islem.isdigit():
        islem_int = int(islem)
        if islem_int not in [1, 2]:
            print('Hatalı işlem girişi')
            continue
        if islem_int == 2:

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')

            kullanici_adi = input('Kullanici Adi Girin :')

            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            if kullanici_adi.strip() == '':
                print('kullanici_adi girmediz')
                continue
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)
            continue
        elif islem_int == 1:
            alt_islem_parola_no = 0
            for parola in parolalar:
                alt_islem_parola_no += 1
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))
            alt_islem = input('Yukarıdakilerden hangisi ?: ')
            if alt_islem.isdigit():
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem):
                    print('Hatalı parola seçimi')
                    continue
                parola = parolalar[int(alt_islem) - 1]
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue

    print('Hatalı giriş yaptınız')
